const dotenv = require('dotenv')
const {Sequelize} = require('sequelize')

dotenv.config()

const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}

const InternalError = "No podemos procesar tu solicitod en estos momentos"

const sequelize= new Sequelize({
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    user: process.env.POSTGRES_USER,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    dialect: 'postgres'
})


module.exports = { redis, InternalError }