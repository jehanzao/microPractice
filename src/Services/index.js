const Controllers = require('../Controllers')

const Create = ({ name, age, color }) => {

    try {
        
        const {statusCode, data, message} = await Controllers.Create({name, color, age})
        
        return {statusCode, data, message};    

    } catch (error) {
        console.log({step: 'service Create', error: error.toString()});

        return {statusCode: 500, message: error.toString()};
    }
}

const Delete = ({ id }) => {

    try {
        
        const {statusCode, data, message} = await Controllers.Delete({where: {id}})
        
        return {statusCode, data, message};    

    } catch (error) {
        console.log({step: 'service Delete', error: error.toString()});

        return {statusCode: 500, message: error.toString()};
    }
}

const Update = ({ name, age, color }) => {

    try {
        
        const {statusCode, data, message} = await Controllers.Update({name, color, age, id})
        
        return {statusCode, data, message};    

    } catch (error) {
        console.log({step: 'service Update', error: error.toString()});

        return {statusCode: 500, message: error.toString()};
    }
}

const FindOne = ({ name }) => {

    try {
        
        const {statusCode, data, message} = await Controllers.FindOne({where: {name}})
        
        return {statusCode, data, message};    

    } catch (error) {
        console.log({step: 'service FindOne', error: error.toString()});

        return {statusCode: 500, message: error.toString()};
    }
}

const View = ({  }) => {

    try {
        
        const {statusCode, data, message} = await Controllers.View({})
        
        return {statusCode, data, message};    

    } catch (error) {
        console.log({step: 'service View', error: error.toString()});

        return {statusCode: 500, message: error.toString()};
    }
}


module.exports = { 
    Create,
    Delete,
    Update,
    FindOne,
    View
 }