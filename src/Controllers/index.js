const { Model } = require('../Models')
const {setTimeout} = require('timers/promises')

const Create = async({name, age, color}) => {
    try {

        const instance = await Model.create(
            {name, age, color}, 
            {fields: ['name', 'age','color']
        })

        return {statusCode: 200, data: instance.toJSON()}

    } catch (error) {
        console.log({step: 'controller Create',error: error.toString()});

        return { stratusCode: 400, message: error.toString()}
    }
}

const Delete = async({where: {}}) => {
    try {
        
        await Model.destroy({ where })

        return {statusCode: 200, data: "Ok"}

    } catch (error) {
        console.log({step: 'controller Delete',error: error.toString()});

        return { stratusCode: 500, message: error.toString()}
    }
}

const Update = async({name, age, color, id}) => {
    try {
        
        const instance = await Model.update(
            {name, age, color},
            { where: { id }}
        )

        return {statusCode: 200, data: instance[1][0].toJSON()}

    } catch (error) {
        console.log({step: 'controller Update',error: error.toString()});

        return { stratusCode: 400, message: error.toString()}
    }
}

const FindOne = async({where: {}}) => {
    try {
        
        const instance = await Model.findOne({where})

        return {statusCode: 200, data: instance.toJSON()}

    } catch (error) {
        console.log({step: 'controller FindOne',error: error.toString()});

        return { stratusCode: 500, message: error.toString()}
    }
}

const View = async({where: {}}) => {
    try {
        
        const instances = await Model.findAll({where})

        return {statusCode: 200, data: instances.toJSON()}
    } catch (error) {
        console.log({step: 'controller View',error: error.toString()});

        return { stratusCode: 500, message: error.toString()}
    }
}



module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View
}