const  Services = require('../Services')
const { InternalError} = require('../settings')

const {queueView, queueCreate, queueDelete, queueFindOne, queueUpdate} = require('./index')

queueCreate.process(async(job, done) => {
    
    try {

        const {name, age, color} = job.data

        const {statusCode, data, message} = await Services.Create({name, age, color})
        // siempre hay que devolver un objeto con la siguiente structura {statusCode, data, message}
        done(null, {statusCode, data, message});

    } catch (error) {

        console.log({step: 'adapter queueCreate', error: error.toString()});
        done(null, {statusCode: 500, message: InternalError});
    }
});

queueDelete.process(async(job, done) => {
    
    try {

        const {id} = job.data

        const {statusCode, data, message} = await Services.Delete({id})
        // siempre hay que devolver un objeto con la siguiente structura {statusCode, data, message}
        done(null, {statusCode, data, message});

    } catch (error) {

        console.log({step: 'adapter queueDelete', error: error.toString()});
        done(null, {statusCode: 500, message: InternalError});
    }
});

queueFindOne.process(async(job, done) => {
    
    try {

        const {name} = job.data

        const {statusCode, data, message} = await Services.FindOne({name})
        // siempre hay que devolver un objeto con la siguiente structura {statusCode, data, message}
        done(null, {statusCode, data, message});

    } catch (error) {

        console.log({step: 'adapter queueFindOne', error: error.toString()});
        done(null, {statusCode: 500, message: InternalError});
    }
});

queueUpdate.process(async(job, done) => {
    
    try {

        const {name, color, age, id} = job.data

        const {statusCode, data, message} = await Services.Update({name, color, age, id})
        // siempre hay que devolver un objeto con la siguiente structura {statusCode, data, message}
        done(null, {statusCode, data, message});

    } catch (error) {

        console.log({step: 'adapter queueUpdate', error: error.toString()});
        done(null, {statusCode: 500, message: InternalError});
    }
});

queueView.process(async(job, done) => {
    
    try {

        const {id} = job.data

        const {statusCode, data, message} = await Services.View({id})
        // siempre hay que devolver un objeto con la siguiente structura {statusCode, data, message}
        done(null, {statusCode, data, message});

    } catch (error) {

        console.log({step: 'adapter queueView', error: error.toString()});
        done(null, {statusCode: 500, message: InternalError});
    }
});