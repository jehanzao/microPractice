const bull = require("bull")
const {redis} = require('../settings')

console.log("ready to work");

const options = {redis: {host: redis.host, port: redis.port}}

const queueCreate = bull('curso:create', options)
const queueDelete = bull('curso:delete', options)
const queueUpdate = bull('curso:update', options)
const queueFindOne = bull('curso:findOne', options)
const queueView = bull('curso:view', options)

module.exports = {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueFindOne,
    queueView
}