const {
    queueCreate, 
    queueDelete, 
    queueFindOne, 
    queueUpdate, 
    queueView} = require('../src/Adapters/index')

const Create = async() => {
    try {
        const job = queueCreate.add({name, age, color})

    const result = await job.finished()

    console.log(result);

    if(result.statusCode === 200) console.log("solicitud correcta");

    } catch (error) {
        
        console.log(error);

    }
}

const Delete = async() => {
    try {
        const job = queueDelete.add({id})

    const result = await job.finished()

    console.log(result);

    if(result.statusCode === 200) console.log("solicitud correcta");

    } catch (error) {
        
        console.log(error);
        
    }
}

const Update = async() => {
    try {
        const job = queueUpdate.add({name, age, color, id})

    const result = await job.finished()

    console.log(result);

    if(result.statusCode === 200) console.log("solicitud correcta");

    } catch (error) {
        
        console.log(error);
        
    }
}

const FindOne = async() => {
    try {
        const job = queueFindOne.add({name})

    const result = await job.finished()

    console.log(result);

    if(result.statusCode === 200) console.log("solicitud correcta");

    } catch (error) {
        
        console.log(error);
        
    }
}

const View = async() => {
    try {
        const job = queueView.add({})

    const result = await job.finished()

    console.log(result);

    if(result.statusCode === 200) console.log("solicitud correcta");

    } catch (error) {
        
        console.log(error);
        
    }
}

const main = async() => {
    View()
}

main()